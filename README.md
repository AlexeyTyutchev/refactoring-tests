# Tidal recruiting refactoring test

##Intro
The code in this project is a isolated and very simplified version of our add-track-to-playlist business code.

###Setup notes
- We have used Guice for dependency injection (in the real service we currently use EJB injection), for the setup to be simpler.
- We have added a fake PlaylistDaoBean that only returns a hardcoded playlist for simplicity.

###Checking out the code
When you are about to check out the project we need you to fork it to your own Github or Bitbucket account so that your commits wont be exposed to other potential candidates we want to perform the test on. 

##Assignments
1. Add necessary unit tests to the existing code to both see that the code works as expected, and to avoid breaking the code when refactoring
2. Refactor the existing code until you are satisfied with it
3. Add a delete track method with appropriate unit tests

##Feel you are done or have questions?
When you have managed to make yourself believe that you are done, please send us a link to your repository so that we may download and evaluate the code. If you have any questions about the test or you want to send your repository link to us, please send it to eivind.hognestad@tidal.com and ruben.chadien@tidal.com with thor.inge.schon@tidal.com on cc.

###One last thing..
Just as important as the refactoring and testing itself - please come with comments on the test project, the setup of it or other things you think can improve or change. Also documentation on what you have done, and why is going to make the evaluation easier for us, and _may_ count as a plus. It's up to you if you decide to document in a separate file, or in the code itself. 

Enjoy!

------------
## Not so visible changes
- Removed double variable initializations
- Removed empty constructors
- Replaced properties that can calculated from available data (Playlists duration/noOfTracks)  
- Added gradle build. (Just if someone wants to use it instead. - Look, how short it is! :) )
- Added master/detail properties synchronization between PlayList/PlayListTrack
- Added validation of tracks to add (Do not waste resources if tracks list is null or empty)
- Added public to all 3 "API" methods of PlayList
 

## Proposals 
- Add generic validation for beans (JSR 303). 
  
  Playlist example:

```
#!java


  public class PlayList {
  
      @NotNull
      private Integer id;

      @NotNull
      private String playListName;

      @Size(max = 500)
      private Set<PlayListTrack> playListTracks;

      @Past          
      private Date registeredDate;

      @Past          
      private Date lastUpdated;

      @NotNull
      private String uuid;

      private boolean deleted;
  }  
  
  Set<Constraintviolation<PlayList>> violations = validator.validate($PLAYLIST_INSTANCE);

```

- Add logging framework (sl4j for ex.) to avoid e.printStackTrace();


## Proposals that CAN be done if I had more information regarding usage of API
- It is not obvious what should be done in case if toIndex is invalid... because first it is adjusted to point to end of playlist, 
  but then result is discarded and adding is not happening. I fixed the code with idea of adding to end of list if toIndex is invalid  
- Changing playlist's property from Set<PlayListTrack> playListTracks to List<PlayListTrack> playListTracks = new ArrayList<> 
  will allow to remove index property of PlayListTrack, because ordering will be stored by List implementation. 
  AND GENERALLY WILL MAKE LIFE EASIER!!! MUUUUCH EASIER  
  Also all the renumbering logic can be dropped then. I have not done this because data package I consider as module's API. 
  So these changes should be decided with teams who use the API.
- Removing of methods marked as deprecated (I don't dare without discussions with persons who use API) 
- Why Playlist has both ID and UUID? 
- Rename duration property to durationSeconds. (If applicable)
- Remove trackId from PlayListTrack - having both can potentially cause de-synchronization. But this depends on what 
  technology is used in real life to persist playlist with tracks...
- Discuss why as the result of adding tracks to playlist should be list of PlaylistTracks