package com.tidal.refactoring.playlist.data;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


/**
 * A very simplified version of TrackPlaylist
 */
public class PlayList {

    private Integer id;
    private String playListName;
    private Set<PlayListTrack> playListTracks = new HashSet<>();
    private Date registeredDate;
    private Date lastUpdated;
    private String uuid;
    private boolean deleted;


    public PlayList() {
        this.uuid = UUID.randomUUID().toString();
        Date d = new Date();
        this.registeredDate = d;
        this.lastUpdated = d;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPlayListName() {
        return playListName;
    }

    public void setPlayListName(String playListName) {
        this.playListName = playListName;
    }

    public Set<PlayListTrack> getPlayListTracks() {
        return playListTracks;
    }

    /*
     * Nice helper method
     */
    public void addTrack(PlayListTrack track) {
        track.setTrackPlaylist(this);
        playListTracks.add(track);
    }

    public void setPlayListTracks(Set<PlayListTrack> playListTracks) {
        this.playListTracks = playListTracks;

        if (this.playListTracks != null) {
            this.playListTracks.forEach(t -> t.setTrackPlaylist(this));
        }
    }

    public Date getRegisteredDate() {
        return registeredDate;
    }

    public void setRegisteredDate(Date registeredDate) {
        this.registeredDate = registeredDate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getNrOfTracks() {
        return playListTracks != null ? playListTracks.size() : 0;
    }

    public Float getDuration() {
        Float res = 0f;

        if (playListTracks != null) {
            for (PlayListTrack track : playListTracks) {
                res += track.getTrack().getDuration();
            }
        }

        return res;
    }


    @Deprecated
    public void setDuration(Float duration) {}

    @Deprecated
    public void setNrOfTracks(int nrOfTracks) {}
}