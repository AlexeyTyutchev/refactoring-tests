package com.tidal.refactoring.playlist;

import com.google.inject.Inject; 
import com.tidal.refactoring.playlist.dao.PlaylistDaoBean;
import com.tidal.refactoring.playlist.data.PlayListTrack;
import com.tidal.refactoring.playlist.data.Track;
import com.tidal.refactoring.playlist.data.PlayList;
import com.tidal.refactoring.playlist.exception.PlaylistException;

import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class PlaylistBusinessBean {

    private PlaylistDaoBean playlistDaoBean;

    public static int MAX_PLAYLIST_SIZE = 500;
    public static int PLAYLIST_FIRST_TRACK_INDEX = 0;

    @Inject
    public PlaylistBusinessBean(PlaylistDaoBean playlistDaoBean){
        this.playlistDaoBean = playlistDaoBean;
    }


    /**
     * Add single track to the index
     */
    public List<PlayListTrack> addTrack(String uuid, Track trackToAdd, int toIndex) throws PlaylistException {
        return addTracks(uuid, Collections.singletonList(trackToAdd), toIndex);
    }


    boolean playlistCanBeExpanded(int currentSize, int expansionSize) {
        return currentSize + expansionSize <= MAX_PLAYLIST_SIZE;
    }

    boolean isPositionValid(int currentSize, int position) {
        return position >= 0 && position < currentSize;
    }

    int adjustPosition(int currentSize, int toIndex) {
        return isPositionValid(currentSize, toIndex) ? toIndex: currentSize;
    }

    List<PlayListTrack> tracksToList(Set<PlayListTrack> tracks) {
        List<PlayListTrack> res = (tracks == null || tracks.size() == 0) ? new ArrayList<>() : new ArrayList<>(tracks);
        Collections.sort(res);
        return res;
    }

    PlayListTrack convertTrack(Track track, PlayList playlist) {
        PlayListTrack res = new PlayListTrack();
        res.setTrack(track);
        res.setTrackPlaylist(playlist);
        res.setDateAdded(new Date());
        res.setTrack(track);

        return res;
    }

    void reindexTracks (List<PlayListTrack> tracks) {
        int i = PLAYLIST_FIRST_TRACK_INDEX;
        for (PlayListTrack track : tracks) {
            track.setIndex(i);
            i++;
        }
    }

    void applyTracksToPlayList(List<PlayListTrack> tracks, PlayList pl) {
        reindexTracks(tracks);
        pl.getPlayListTracks().clear();
        pl.getPlayListTracks().addAll(tracks);
    }

    void mergeTracksIntoPlaylist(List<PlayListTrack> tracks, PlayList pl, int index) {
        List<PlayListTrack> currentPlayListTracks = tracksToList(pl.getPlayListTracks());
        currentPlayListTracks.addAll(index, tracks);
        applyTracksToPlayList(currentPlayListTracks, pl);
    }

    /**
     * Add tracks to the index
     */
    public List<PlayListTrack> addTracks(String uuid, List<Track> tracksToAdd, int toIndex) throws PlaylistException {
        if (tracksToAdd == null || tracksToAdd.size() == 0) return Collections.emptyList();

        try {
            PlayList playList = playlistDaoBean.getPlaylistByUUID(uuid);

            //We do not allow > 500 tracks in new playlists
            if (!playlistCanBeExpanded(playList.getNrOfTracks(), tracksToAdd.size())) {
                throw new PlaylistException("Playlist cannot have more than " + MAX_PLAYLIST_SIZE + " tracks");
            }


            // The index is out of bounds, put it in the end of the list.
            toIndex = adjustPosition(playList.getNrOfTracks(), toIndex);

            List<PlayListTrack> convertedTracks = tracksToAdd.stream().map(track -> convertTrack(track, playList)).collect(Collectors.toList());

            mergeTracksIntoPlaylist(convertedTracks, playList, toIndex);

            return convertedTracks;
        }
        catch (PlaylistException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace(); // :(
            throw new PlaylistException("Generic error");
        }
    }

	/**
	 * Remove the tracks from the playlist located at the sent indexes
	 */
    public List<PlayListTrack> removeTracks(String uuid, List<Integer> indexes) throws PlaylistException {
        if (indexes == null || indexes.size() == 0) return Collections.emptyList();

        try {
            if (indexes.size() != new HashSet<>(indexes).size()) {
                throw new PlaylistException("Track indexes list has duplicates");
            }


            PlayList playList = playlistDaoBean.getPlaylistByUUID(uuid);
            for (Integer index : indexes) {
                if (!isPositionValid(playList.getNrOfTracks(), index)) {
                    throw new PlaylistException("Invalid track position: " + index);
                }
            }

            List<PlayListTrack> tracks = tracksToList(playList.getPlayListTracks());

            List<PlayListTrack> tracksToRemove = new ArrayList<>();

            for (Integer i : indexes) {
                tracksToRemove.add(tracks.get(i));
            }

            List<Integer> indexesToRemove = new ArrayList<>(indexes);
            indexesToRemove.sort(Comparator.reverseOrder());
            indexesToRemove.forEach(i -> tracks.remove(i.intValue()));


            // To store it after change
            applyTracksToPlayList(tracks, playList);

            return tracksToRemove;
        }
        catch (PlaylistException e) {
            throw e;
        }
        catch (Exception e) {
            e.printStackTrace(); // :( :( :(
            throw new PlaylistException("Generic error");
        }
	}
}
