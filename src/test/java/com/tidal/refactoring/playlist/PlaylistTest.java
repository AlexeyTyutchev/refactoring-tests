package com.tidal.refactoring.playlist;

import com.tidal.refactoring.playlist.data.PlayList;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class PlaylistTest {

    @Test
    public void testNoOfTracks() {
        PlayList pl = new PlayList();

        assertEquals(pl.getNrOfTracks(), 0);

        pl.addTrack(Builder.buildTestTrack(1, 1000));
        pl.addTrack(Builder.buildTestTrack(2, 10000));

        assertEquals(pl.getNrOfTracks(), 2);

    }


    @Test
    public void testDuration() {
        PlayList pl = new PlayList();

        assertEquals(pl.getDuration(), 0f);

        pl.addTrack(Builder.buildTestTrack(1, 1000));
        pl.addTrack(Builder.buildTestTrack(2, 10000));

        assertEquals(pl.getDuration(), 11000f);

    }
}
