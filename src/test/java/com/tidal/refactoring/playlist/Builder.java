package com.tidal.refactoring.playlist;

import com.tidal.refactoring.playlist.data.PlayListTrack;
import com.tidal.refactoring.playlist.data.Track;


public class Builder {
    public static PlayListTrack buildTestTrack(int index) {
        return buildTestTrack(index, 10000);
    }

    public static PlayListTrack buildTestTrack(int index, float duration) {
        PlayListTrack res = new PlayListTrack();
        res.setTrack(new Track());
        res.getTrack().setDuration(duration);
        res.getTrack().setArtistId(1);
        res.getTrack().setTitle("Some obvious track name... Like NoName 1");
        res.setIndex(index);
        return res;
    }
}