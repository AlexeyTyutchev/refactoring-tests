package com.tidal.refactoring.playlist;

import com.google.inject.Inject;
import com.tidal.refactoring.playlist.dao.PlaylistDaoBean;
import com.tidal.refactoring.playlist.data.PlayList;
import com.tidal.refactoring.playlist.data.PlayListTrack;
import com.tidal.refactoring.playlist.data.Track;
import com.tidal.refactoring.playlist.exception.PlaylistException;
import org.testng.annotations.Guice;
import org.testng.annotations.Test;

import java.util.*;

import static org.testng.Assert.*;


@Guice(modules = TestBusinessModule.class)
public class PlaylistBusinessBeanTest {

    @Inject
    private PlaylistBusinessBean playlistBusinessBean;


    @Test
    public void testAddTracks() {
        List<Track> trackList = new ArrayList<>();

        Track track = new Track();
        track.setArtistId(4);
        track.setTitle("A brand new track");
        track.setId(76868);

        trackList.add(track);

        track = new Track();
        track.setArtistId(4);
        track.setTitle("Not so brand new track");
        track.setId(76869);

        trackList.add(track);

        List<PlayListTrack> playListTracks = playlistBusinessBean.addTracks(UUID.randomUUID().toString(), trackList, 4);

        assertEquals(playListTracks.size(), 2);
    }

    @Test
    public void testAddTrack() {
        Track track = new Track();
        track.setArtistId(4);
        track.setTitle("A final version of brand new track");
        track.setId(76868);

        List<PlayListTrack> playListTracks = playlistBusinessBean.addTrack(UUID.randomUUID().toString(), track, 5);
        assertEquals(playListTracks.size(), 1);
        assertEquals(playListTracks.get(0).getIndex(), 5);

        playListTracks = playlistBusinessBean.addTrack(UUID.randomUUID().toString(), track, 0);
        assertEquals(playListTracks.size(), 1);
        assertEquals(playListTracks.get(0).getIndex(), 0);

        playListTracks = playlistBusinessBean.addTrack(UUID.randomUUID().toString(), track, 100500);
        assertEquals(playListTracks.size(), 1);
        assertEquals(playListTracks.get(0).getIndex(), PlaylistDaoBean.NUMBER_OF_TRACKS);
    }

    @Test(expectedExceptions = PlaylistException.class)
    public void testAddTooManyTracks() {
        List<Track> tracks = new ArrayList<>();
        for (int i = 0; i < 1 + PlaylistBusinessBean.MAX_PLAYLIST_SIZE - PlaylistDaoBean.NUMBER_OF_TRACKS; i++) {
            Track track = new Track();
            track.setArtistId(4);
            track.setTitle("Brand new track - Black metal version");
            track.setId(76868);
            tracks.add(track);
        }

        playlistBusinessBean.addTracks(UUID.randomUUID().toString(), tracks, 0);
    }

    @Test
    public void testAddRightOkNumberOfTracks() {
        List<Track> tracks = new ArrayList<>();
        for (int i = 0; i < PlaylistBusinessBean.MAX_PLAYLIST_SIZE - PlaylistDaoBean.NUMBER_OF_TRACKS; i++) {
            Track track = new Track();
            track.setArtistId(4);
            track.setTitle("Funeral march (Happy hardcore remix)");
            track.setId(76868);
            tracks.add(track);
        }

        List<PlayListTrack> playListTracks = playlistBusinessBean.addTracks(UUID.randomUUID().toString(), tracks, 100500);
        assertEquals(playListTracks.get(playListTracks.size() - 1).getIndex(), PlaylistBusinessBean.MAX_PLAYLIST_SIZE - 1);
    }

    @Test
    public void testSizes() {
        assertTrue(playlistBusinessBean.playlistCanBeExpanded(0, 500));
        assertTrue(playlistBusinessBean.playlistCanBeExpanded(1, 499));
        assertTrue(playlistBusinessBean.playlistCanBeExpanded(250, 250));
        assertTrue(playlistBusinessBean.playlistCanBeExpanded(499, 1));
        assertTrue(playlistBusinessBean.playlistCanBeExpanded(500, 0));
    }

    @Test
    public void testPositionAdjustment() {
        assertEquals(playlistBusinessBean.adjustPosition(0, 500), 0);
        assertEquals(playlistBusinessBean.adjustPosition(0, -1), 0);
        assertEquals(playlistBusinessBean.adjustPosition(200, 2), 2);
        assertEquals(playlistBusinessBean.adjustPosition(200, 198), 198);
        assertEquals(playlistBusinessBean.adjustPosition(200, 201), 200);
    }

    @Test
    public void testTracksSetToListConversion() {
        PlayList pl = new PlayList();

        pl.addTrack(Builder.buildTestTrack(10, 1000));
        pl.addTrack(Builder.buildTestTrack(34));
        pl.addTrack(Builder.buildTestTrack(100500));
        pl.addTrack(Builder.buildTestTrack(1));
        pl.addTrack(Builder.buildTestTrack(0));
        pl.addTrack(Builder.buildTestTrack(-1000));

        List<PlayListTrack> playListTracks = playlistBusinessBean.tracksToList(pl.getPlayListTracks());

        for (int i = 0; i < playListTracks.size() - 1; i++) {
            assertTrue(playListTracks.get(i).getIndex() <= playListTracks.get(i + 1).getIndex());
        }
    }

    @Test
    public void testTracksReindexing() {
        PlayList pl = new PlayList();

        pl.addTrack(Builder.buildTestTrack(10));
        pl.addTrack(Builder.buildTestTrack(34));
        pl.addTrack(Builder.buildTestTrack(100500));
        pl.addTrack(Builder.buildTestTrack(1));
        pl.addTrack(Builder.buildTestTrack(0));
        pl.addTrack(Builder.buildTestTrack(-1000));

        List<PlayListTrack> playListTracks = playlistBusinessBean.tracksToList(pl.getPlayListTracks());

        playlistBusinessBean.reindexTracks(playListTracks);

        assertEquals(playListTracks.get(0).getIndex(), PlaylistBusinessBean.PLAYLIST_FIRST_TRACK_INDEX);

        for (int i = 0; i < playListTracks.size() - 1; i++) {
            assertEquals(playListTracks.get(i).getIndex() + 1, playListTracks.get(i + 1).getIndex());
        }
    }

    @Test
    public void testTracksAdding() {
        PlayList pl = new PlayList();

        pl.addTrack(Builder.buildTestTrack(10));
        pl.addTrack(Builder.buildTestTrack(34));
        pl.addTrack(Builder.buildTestTrack(100500));
        pl.addTrack(Builder.buildTestTrack(1));
        pl.addTrack(Builder.buildTestTrack(0));
        pl.addTrack(Builder.buildTestTrack(-1000));

        List<PlayListTrack> tracksToAdd = new ArrayList<>();
        tracksToAdd.add(Builder.buildTestTrack(2));
        tracksToAdd.add(Builder.buildTestTrack(3));
        tracksToAdd.add(Builder.buildTestTrack(4));
        tracksToAdd.add(Builder.buildTestTrack(5));

        playlistBusinessBean.mergeTracksIntoPlaylist(tracksToAdd, pl, 0);

        assertEquals(pl.getNrOfTracks(), 10);
    }

    @Test(expectedExceptions = PlaylistException.class)
    public void testTracksRemovalWithNegativeIndex() {
        playlistBusinessBean.removeTracks(UUID.randomUUID().toString(), Collections.singletonList(-100));
    }

    @Test(expectedExceptions = PlaylistException.class)
    public void testTracksRemovalWithTooBigIndex() {
        playlistBusinessBean.removeTracks(UUID.randomUUID().toString(), Collections.singletonList(PlaylistDaoBean.NUMBER_OF_TRACKS));
    }


    @Test(expectedExceptions = PlaylistException.class)
    public void testTracksRemovalWithNonUniqueIndex() {
        List<Integer> indexes = Arrays.asList(0, 5, 5, 100, 350, PlaylistDaoBean.NUMBER_OF_TRACKS - 1);
        playlistBusinessBean.removeTracks(UUID.randomUUID().toString(), indexes);
    }

    @Test
    public void testTracksRemoval() {
        List<List<Integer>> tests = Arrays.asList(
            Arrays.asList(0, 5, 100, 350, PlaylistDaoBean.NUMBER_OF_TRACKS - 1),
            Arrays.asList(PlaylistDaoBean.NUMBER_OF_TRACKS - 1, 350, 100, 5, 0),
            Arrays.asList(350, PlaylistDaoBean.NUMBER_OF_TRACKS - 1, 0, 5, 100)
        );

        for (List<Integer> indexes : tests) {
            List<PlayListTrack> removedTracks = playlistBusinessBean.removeTracks(UUID.randomUUID().toString(), indexes);

            for (int i = 0; i <indexes.size(); i++) {
                assertEquals(indexes.get(i).intValue(), removedTracks.get(i).getIndex());
            }
        }
    }
}